<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cnv_tipo_documento".
 *
 * @property int $id_tipo_documento
 * @property string $nombre_tipo_documento
 * @property string $descripcion
 * @property string $vigente
 */
class CnvTipoDocumento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cnv_tipo_documento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_tipo_documento'], 'required'],
            [['id_tipo_documento'], 'default', 'value' => null],
            [['id_tipo_documento'], 'integer'],
            [['nombre_tipo_documento'], 'string', 'max' => 200],
            [['descripcion'], 'string', 'max' => 500],
            [['vigente'], 'string', 'max' => 1],
            [['id_tipo_documento'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tipo_documento' => 'Id Tipo Documento',
            'nombre_tipo_documento' => 'Nombre Tipo Documento',
            'descripcion' => 'Descripcion',
            'vigente' => 'Vigente',
        ];
    }
}
