<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cnv_convenio_institucion".
 *
 * @property integer $id_institucion
 * @property integer $id_convenio
 *
 * @property CnvConvenio $idConvenio
 * @property CnvInstitucion $idInstitucion
 */
class CnvConvenioInstitucion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cnv_convenio_institucion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_institucion', 'id_convenio'], 'required'],
            [['id_institucion', 'id_convenio'], 'integer'],
            [['id_convenio'], 'exist', 'skipOnError' => true, 'targetClass' => CnvConvenio::className(), 'targetAttribute' => ['id_convenio' => 'id_convenio']],
            [['id_institucion'], 'exist', 'skipOnError' => true, 'targetClass' => CnvInstitucion::className(), 'targetAttribute' => ['id_institucion' => 'id_institucion']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_institucion' => 'Id Institucion',
            'id_convenio' => 'Id Convenio',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdConvenio()
    {
        return $this->hasOne(CnvConvenio::className(), ['id_convenio' => 'id_convenio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdInstitucion()
    {
        return $this->hasOne(CnvInstitucion::className(), ['id_institucion' => 'id_institucion']);
    }
}
