<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cnv_convenio".
 *
 * @property int $id_convenio
 * @property int $id_tipo_convenio
 * @property string $id_coordinador_convenio
 * @property int $id_estado_convenio
 * @property string $nombre_convenio
 * @property string $fecha_inicio
 * @property string $fecha_termino
 * @property string $fecha_firma
 * @property string $fecha_decreto
 * @property int $numero_decreto
 * @property string $descripcion
 * @property string $vigente
 * @property int $vigencia
 *
 * @property CnvActividadConvenio[] $cnvActividadConvenios
 * @property CnvCoordinadorConvenio $coordinadorConvenio
 * @property CnvEstadoConvenio $estadoConvenio
 * @property CnvConvenioInstitucion[] $cnvConvenioInstitucions
 * @property CnvModalidadColaboracionConv[] $cnvModalidadColaboracionConvs
 * @property CnvModalidadConvenio[] $cnvModalidadConvenios
 * @property CnvObjetivoConvenio[] $cnvObjetivoConvenios
 */
class CnvConvenio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cnv_convenio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_convenio', 'id_tipo_convenio', 'id_estado_convenio'], 'required'],
            [['id_convenio', 'id_tipo_convenio', 'id_estado_convenio', 'numero_decreto', 'vigencia'], 'default', 'value' => null],
            [['id_convenio', 'id_tipo_convenio', 'id_estado_convenio', 'numero_decreto', 'vigencia'], 'integer'],
            [['fecha_inicio', 'fecha_termino', 'fecha_firma', 'fecha_decreto'], 'safe'],
            [['id_coordinador_convenio'], 'string', 'max' => 20],
            [['nombre_convenio', 'descripcion'], 'string', 'max' => 500],
            [['vigente'], 'string', 'max' => 1],
            [['id_convenio'], 'unique'],
            [['id_coordinador_convenio'], 'exist', 'skipOnError' => true, 'targetClass' => CnvCoordinadorConvenio::className(), 'targetAttribute' => ['id_coordinador_convenio' => 'id_coordinador_convenio']],
            [['id_estado_convenio'], 'exist', 'skipOnError' => true, 'targetClass' => CnvEstadoConvenio::className(), 'targetAttribute' => ['id_estado_convenio' => 'id_estado_convenio']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_convenio' => 'Identificador de Convenio',
            'id_tipo_convenio' => 'Identificador Tipo Convenio',
            'id_coordinador_convenio' => 'Identificador Coordinador Convenio',
            'id_estado_convenio' => 'Identificador Estado Convenio',
            'nombre_convenio' => 'Nombre de Convenio',
            'fecha_inicio' => 'Fecha de Inicio',
            'fecha_termino' => 'Fecha de Termino',
            'fecha_firma' => 'Fecha de Firma',
            'fecha_decreto' => 'Fecha de Decreto',
            'numero_decreto' => 'Numero de Decreto',
            'descripcion' => 'Descripcion de Convenio',
            'vigente' => 'Vigente (S ó N)',
            'vigencia' => 'Vigencia',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCnvActividadConvenios()
    {
        return $this->hasMany(CnvActividadConvenio::className(), ['id_convenio' => 'id_convenio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoordinadorConvenio()
    {
        return $this->hasOne(CnvCoordinadorConvenio::className(), ['id_coordinador_convenio' => 'id_coordinador_convenio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoConvenio()
    {
        return $this->hasOne(CnvEstadoConvenio::className(), ['id_estado_convenio' => 'id_estado_convenio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCnvConvenioInstitucions()
    {
        return $this->hasMany(CnvConvenioInstitucion::className(), ['id_convenio' => 'id_convenio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCnvModalidadColaboracionConvs()
    {
        return $this->hasMany(CnvModalidadColaboracionConv::className(), ['id_convenio' => 'id_convenio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCnvModalidadConvenios()
    {
        return $this->hasMany(CnvModalidadConvenio::className(), ['id_convenio' => 'id_convenio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCnvObjetivoConvenios()
    {
        return $this->hasMany(CnvObjetivoConvenio::className(), ['id_convenio' => 'id_convenio']);
    }
}
