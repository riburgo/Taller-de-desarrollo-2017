<?php

namespace app\controllers;

use Yii;
use app\models\CnvCoordinadorConvenio;
use app\models\CnvCoordinadorConvenioSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\FormRegister;
use app\models\Users;
use app\models\User;
use app\data\Pagination;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
class SiteController extends Controller
{
   
 private function randKey($str='', $long=0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }

 public function actionRegister()
 {
  //Creamos la instancia con el model de validación
    $this -> layout = 'mainAdmin';
  $model = new FormRegister;
   
  //Mostrará un mensaje en la vista cuando el usuario se haya registrado
  $msg = null;
   
  //Validación mediante ajax
  if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
   
  //Validación cuando el formulario es enviado vía post
  //Esto sucede cuando la validación ajax se ha llevado a cabo correctamente
  //También previene por si el usuario tiene desactivado javascript y la
  //validación mediante ajax no puede ser llevada a cabo
  if ($model->load(Yii::$app->request->post()))
  {
   if($model->validate())
   {
    //Preparamos la consulta para guardar el usuario
    $table = new Users;
    $table->username = $model->username;
    $table->email = $model->email;
    $table->role = $model->role;
    $table->password = $model->password;
    //clave será utilizada para activar el usuario
    $table->authKey = $this->randKey("abcdef0123456789", 15);
    //Creamos un token de acceso único para el usuario
    $table->accessToken = $this->randKey("abcdef0123456789", 15);
    $table->activate = 1;
    //Si el registro es guardado correctamente
    if ($table->insert())
    {
     //Nueva consulta para obtener el id del usuario
     //Para confirmar al usuario se requiere su id y su authKey
   
     
    $model->username = null;
     $model->email = null;
     $model->role = null;
     $model->password = null;
     $model->password_repeat = null;
    
     $msg = "Enhorabuena, ahora tienes tu usuario creado";
    }
     
   }
   else
   {
    $model->getErrors();
   }
  }
  return $this->render("register", ["model" => $model, "msg" => $msg]);
 }
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'register','user', 'admin','coordinador'],
                'rules' => [
                    [
                        //El administrador tiene permisos sobre las siguientes acciones
                        'actions' => ['logout', 'register','admin'],
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un administrador
                            return User::isUserAdmin(Yii::$app->user->identity->id);
                        },
                    ],
                    [
                       //Los usuarios simples tienen permisos sobre las siguientes acciones
                       'actions' => ['logout', 'user'],
                       //Esta propiedad establece que tiene permisos
                       'allow' => true,
                       //Usuarios autenticados, el signo ? es para invitados
                       'roles' => ['@'],
                       //Este método nos permite crear un filtro sobre la identidad del usuario
                       //y así establecer si tiene permisos o no
                       'matchCallback' => function ($rule, $action) {
                          //Llamada al método que comprueba si es un usuario simple
                          return User::isUserSimple(Yii::$app->user->identity->id);
                      },
                   ],
                   [
                       //Los coordinadores tienen permisos sobre las siguientes acciones
                       'actions' => ['logout', 'coordinador'],
                       //Esta propiedad establece que tiene permisos
                       'allow' => true,
                       //Usuarios autenticados, el signo ? es para invitados
                       'roles' => ['@'],
                       //Este método nos permite crear un filtro sobre la identidad del usuario
                       //y así establecer si tiene permisos o no
                       'matchCallback' => function ($rule, $action) {
                          //Llamada al método que comprueba si es un usuario simple
                          return User::isUserCoordinador(Yii::$app->user->identity->id);
                      },
                   ],
                ],
            ],
     //Controla el modo en que se accede a las acciones, en este ejemplo a la acción logout
     //sólo se puede acceder a través del método post
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
      if(Yii::$app->user->isGuest)
                        $this->redirect(array('login'));
                else 
                return $this->render('index');
    }
        public function actionAdmin()
    {
        $this -> layout = 'mainAdmin';
      if(Yii::$app->user->isGuest)
                        $this->redirect(array('login'));
                else 
                return $this->render('admin');
    }

           public function actionCoordinador()
    {
        $this -> layout = 'mainCoord';
      if(Yii::$app->user->isGuest)
                        $this->redirect(array('login'));
                else 
                return $this->render('coordinador');
    }

           public function actionUser()
    {
        $this -> layout = 'mainUser';
      if(Yii::$app->user->isGuest)
                        $this->redirect(array('login'));
                else 
                return $this->render('user');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
  
     public function actionLogin()
    {
        $this -> layout = 'login';
        if (!Yii::$app->user->isGuest) {
   
   if (User::isUserAdmin(Yii::$app->user->identity->id))
   {
    return $this->redirect(["site/admin"]);
   }
   if (User::isUserCoordinador(Yii::$app->user->identity->id))
   {
    return $this->redirect(["site/coordinador"]);
   }if  (User::isUserSimple(Yii::$app->user->identity->id))
    return $this->redirect(["site/user"]);
    }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
   
            if (User::isUserAdmin(Yii::$app->user->identity->id))
   {
    return $this->redirect(["site/admin"]);
   }
    if (User::isUserCoordinador(Yii::$app->user->identity->id))
   {
    return $this->redirect(["site/coordinador"]);
   }
    if (User::isUserSimple(Yii::$app->user->identity->id)){
    return $this->redirect(["site/user"]);
   }
   
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this::redirect('index.php?r=site%2Flogin');

    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $this -> layout = 'mainUser';
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }


    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {   
        $this -> layout = 'mainUser';
        return $this->render('about');
    }

}