<?php

namespace app\controllers;

use Yii;
use app\models\CnvConvenio;
use app\models\CnvConvenioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Users;
use app\models\User;
/**
 * CnvConvenioController implements the CRUD actions for CnvConvenio model.
 */
class CnvConvenioController extends Controller
{
    /**
     * @inheritdoc
     */
 public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [ 'create','update','index','view','user', 'admin','coordinador'],
                'rules' => [
                    [
                        //El administrador tiene permisos sobre las siguientes acciones
                        'actions' => ['create','update','index','view', 'admin'],
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un administrador
                            return User::isUserAdmin(Yii::$app->user->identity->id);
                        },
                    ],
                    [
                       //Los usuarios simples tienen permisos sobre las siguientes acciones
                       'actions' => ['index', 'user'],
                       //Esta propiedad establece que tiene permisos
                       'allow' => true,
                       //Usuarios autenticados, el signo ? es para invitados
                       'roles' => ['@'],
                       //Este método nos permite crear un filtro sobre la identidad del usuario
                       //y así establecer si tiene permisos o no
                       'matchCallback' => function ($rule, $action) {
                          //Llamada al método que comprueba si es un usuario simple
                          return User::isUserSimple(Yii::$app->user->identity->id);
                      },
                   ],
                   [
                       //Los usuarios simples tienen permisos sobre las siguientes acciones
                       'actions' => ['create','update','index', 'view','coordinador'],
                       //Esta propiedad establece que tiene permisos
                       'allow' => true,
                       //Usuarios autenticados, el signo ? es para invitados
                       'roles' => ['@'],
                       //Este método nos permite crear un filtro sobre la identidad del usuario
                       //y así establecer si tiene permisos o no
                       'matchCallback' => function ($rule, $action) {
                          //Llamada al método que comprueba si es un usuario simple
                          return User::isUserCoordinador(Yii::$app->user->identity->id);
                      },
                   ],
                ],
            ],
     //Controla el modo en que se accede a las acciones, en este ejemplo a la acción logout
     //sólo se puede acceder a través del método post
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CnvConvenio models.
     * @return mixed
     */
    public function actionIndex()
    {
    if (User::isUserAdmin(Yii::$app->user->identity->id)){
         $this-> layout = 'mainAdmin';
        $searchModel = new CnvConvenioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    if (User::isUserCoordinador(Yii::$app->user->identity->id)){
         $this-> layout = 'mainCoord';
        $searchModel = new CnvConvenioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    if (User::isUserSimple(Yii::$app->user->identity->id)){
         $this-> layout = 'mainUser';
        $searchModel = new CnvConvenioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}

    /**
     * Displays a single CnvConvenio model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
         if (User::isUserAdmin(Yii::$app->user->identity->id)){
         $this-> layout = 'mainAdmin';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
     if (User::isUserCoordinador(Yii::$app->user->identity->id)){
         $this-> layout = 'mainCoord';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
     if (User::isUserSimple(Yii::$app->user->identity->id)){
         $this-> layout = 'mainUser';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
}

    /**
     * Creates a new CnvConvenio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
         if (User::isUserSimple(Yii::$app->user->identity->id)){
         $this-> layout = 'mainUser';
        $model = new CnvConvenio();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_convenio]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    if (User::isUserCoordinador(Yii::$app->user->identity->id)){
         $this-> layout = 'mainCoord';
        $model = new CnvConvenio();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_convenio]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    if (User::isUserAdmin(Yii::$app->user->identity->id)){
         $this-> layout = 'mainAdmin';
        $model = new CnvConvenio();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_convenio]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
}
    /**
     * Updates an existing CnvConvenio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
         if (User::isUserAdmin(Yii::$app->user->identity->id)){
         $this-> layout = 'mainAdmin';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_convenio]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
     if (User::isUserCoordinador(Yii::$app->user->identity->id)){
         $this-> layout = 'mainCoord';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_convenio]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
     if (User::isUserSimple(Yii::$app->user->identity->id)){
         $this-> layout = 'mainUser';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_convenio]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
}
    /**
     * Deletes an existing CnvConvenio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (User::isUserSimple(Yii::$app->user->identity->id)){
         $this-> layout = 'mainUser';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
       if (User::isUserCoordinador(Yii::$app->user->identity->id)){
         $this-> layout = 'mainCoord';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
       if (User::isUserAdmin(Yii::$app->user->identity->id)){
         $this-> layout = 'mainAdmin';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}

    /**
     * Finds the CnvConvenio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CnvConvenio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CnvConvenio::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
