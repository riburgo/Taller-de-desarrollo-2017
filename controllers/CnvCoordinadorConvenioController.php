<?php

namespace app\controllers;

use Yii;
use app\models\CnvCoordinadorConvenio;
use app\models\CnvCoordinadorConvenioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Users;
use app\models\User;
/**
 * CnvCoordinadorConvenioController implements the CRUD actions for CnvCoordinadorConvenio model.
 */
class CnvCoordinadorConvenioController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [ 'create','update','index','view','user', 'admin','coordinador'],
                'rules' => [
                    [
                        //El administrador tiene permisos sobre las siguientes acciones
                        'actions' => ['create','update','index','view', 'admin'],
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un administrador
                            return User::isUserAdmin(Yii::$app->user->identity->id);
                        },
                    ],
                    [
                       //Los usuarios simples tienen permisos sobre las siguientes acciones
                       'actions' => ['index', 'user'],
                       //Esta propiedad establece que tiene permisos
                       'allow' => true,
                       //Usuarios autenticados, el signo ? es para invitados
                       'roles' => ['@'],
                       //Este método nos permite crear un filtro sobre la identidad del usuario
                       //y así establecer si tiene permisos o no
                       'matchCallback' => function ($rule, $action) {
                          //Llamada al método que comprueba si es un usuario simple
                          return User::isUserSimple(Yii::$app->user->identity->id);
                      },
                   ],
                   [
                       //Los usuarios simples tienen permisos sobre las siguientes acciones
                       'actions' => ['create','update','index','view', 'coordinador'],
                       //Esta propiedad establece que tiene permisos
                       'allow' => true,
                       //Usuarios autenticados, el signo ? es para invitados
                       'roles' => ['@'],
                       //Este método nos permite crear un filtro sobre la identidad del usuario
                       //y así establecer si tiene permisos o no
                       'matchCallback' => function ($rule, $action) {
                          //Llamada al método que comprueba si es un usuario simple
                          return User::isUserCoordinador(Yii::$app->user->identity->id);
                      },
                   ],
                ],
            ],
     //Controla el modo en que se accede a las acciones, en este ejemplo a la acción logout
     //sólo se puede acceder a través del método post
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Lists all CnvCoordinadorConvenio models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (User::isUserAdmin(Yii::$app->user->identity->id)){
         $this-> layout = 'mainAdmin';
        $searchModel = new CnvCoordinadorConvenioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    if (User::isUserCoordinador(Yii::$app->user->identity->id)){
         $this-> layout = 'mainCoord';
        $searchModel = new CnvCoordinadorConvenioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    if (User::isUserSimple(Yii::$app->user->identity->id)){
         $this-> layout = 'mainUser';
        $searchModel = new CnvCoordinadorConvenioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
    /**
     * Displays a single CnvCoordinadorConvenio model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (User::isUserSimple(Yii::$app->user->identity->id)){
         $this-> layout = 'mainUser';
       //  $this -> layout = 'main2';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
       if (User::isUserCoordinador(Yii::$app->user->identity->id)){
         $this-> layout = 'mainCoord';
       //  $this -> layout = 'main2';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
       if (User::isUserAdmin(Yii::$app->user->identity->id)){
         $this-> layout = 'mainAdmin';
       //  $this -> layout = 'main2';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
}

    /**
     * Creates a new CnvCoordinadorConvenio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (User::isUserAdmin(Yii::$app->user->identity->id)){
         $this-> layout = 'mainAdmin';
        // $this -> layout = 'main2';
        $model = new CnvCoordinadorConvenio();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_coordinador_convenio]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    if (User::isUserCoordinador(Yii::$app->user->identity->id)){
         $this-> layout = 'mainCoord';
        // $this -> layout = 'main2';
        $model = new CnvCoordinadorConvenio();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_coordinador_convenio]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    if (User::isUserSimple(Yii::$app->user->identity->id)){
         $this-> layout = 'mainUser';
        // $this -> layout = 'main2';
        $model = new CnvCoordinadorConvenio();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_coordinador_convenio]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
}
    /**
     * Updates an existing CnvCoordinadorConvenio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
         if (User::isUserSimple(Yii::$app->user->identity->id)){
         $this-> layout = 'mainUser';
      //   $this -> layout = 'main2';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_coordinador_convenio]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
      if (User::isUserCoordinador(Yii::$app->user->identity->id)){
         $this-> layout = 'mainCoord';
      //   $this -> layout = 'main2';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_coordinador_convenio]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
      if (User::isUserAdmin(Yii::$app->user->identity->id)){
         $this-> layout = 'mainAdmin';
      //   $this -> layout = 'main2';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_coordinador_convenio]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
}

    /**
     * Deletes an existing CnvCoordinadorConvenio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
     if (User::isUserAdmin(Yii::$app->user->identity->id)){
         $this-> layout = 'mainAdmin';
    //     $this -> layout = 'main2';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
     if (User::isUserCoordinador(Yii::$app->user->identity->id)){
         $this-> layout = 'mainCoord';
    //     $this -> layout = 'main2';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
     if (User::isUserSimple(Yii::$app->user->identity->id)){
         $this-> layout = 'mainUser';
    //     $this -> layout = 'main2';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}
    /**
     * Finds the CnvCoordinadorConvenio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CnvCoordinadorConvenio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
       //s  $this -> layout = 'main2';
        if (($model = CnvCoordinadorConvenio::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
