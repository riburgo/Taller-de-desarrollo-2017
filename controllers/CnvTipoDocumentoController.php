<?php

namespace app\controllers;

use Yii;
use app\models\CnvTipoDocumento;
use app\models\CnvTipoDocumentoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Users;
use app\models\User;
/**
 * CnvTipoDocumentoController implements the CRUD actions for CnvTipoDocumento model.
 */
class CnvTipoDocumentoController extends Controller
{
    /**
     * @inheritdoc
     */

public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [ 'create','update','index','view','user', 'admin','coordinador'],
                'rules' => [
                    [
                        //El administrador tiene permisos sobre las siguientes acciones
                        'actions' => ['create','update','index','view', 'admin'],
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un administrador
                            return User::isUserAdmin(Yii::$app->user->identity->id);
                        },
                    ],
                    [
                       //Los usuarios simples tienen permisos sobre las siguientes acciones
                       'actions' => ['index', 'user'],
                       //Esta propiedad establece que tiene permisos
                       'allow' => true,
                       //Usuarios autenticados, el signo ? es para invitados
                       'roles' => ['@'],
                       //Este método nos permite crear un filtro sobre la identidad del usuario
                       //y así establecer si tiene permisos o no
                       'matchCallback' => function ($rule, $action) {
                          //Llamada al método que comprueba si es un usuario simple
                          return User::isUserSimple(Yii::$app->user->identity->id);
                      },
                   ],
                   [
                       //Los usuarios simples tienen permisos sobre las siguientes acciones
                       'actions' => ['create','update','index', 'view','coordinador'],
                       //Esta propiedad establece que tiene permisos
                       'allow' => true,
                       //Usuarios autenticados, el signo ? es para invitados
                       'roles' => ['@'],
                       //Este método nos permite crear un filtro sobre la identidad del usuario
                       //y así establecer si tiene permisos o no
                       'matchCallback' => function ($rule, $action) {
                          //Llamada al método que comprueba si es un usuario simple
                          return User::isUserCoordinador(Yii::$app->user->identity->id);
                      },
                   ],
                ],
            ],
     //Controla el modo en que se accede a las acciones, en este ejemplo a la acción logout
     //sólo se puede acceder a través del método post
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CnvTipoDocumento models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (User::isUserSimple(Yii::$app->user->identity->id)){
            $this-> layout = 'mainUser';
        $searchModel = new CnvTipoDocumentoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    if (User::isUserCoordinador(Yii::$app->user->identity->id)){
            $this-> layout = 'mainCoord';
        $searchModel = new CnvTipoDocumentoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    if (User::isUserAdmin(Yii::$app->user->identity->id)){
            $this-> layout = 'mainAdmin';
        $searchModel = new CnvTipoDocumentoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}

    /**
     * Displays a single CnvTipoDocumento model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
     if (User::isUserAdmin(Yii::$app->user->identity->id)){
            $this-> layout = 'mainAdmin';
     
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
      if (User::isUserCoordinador(Yii::$app->user->identity->id)){
            $this-> layout = 'mainCoord';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
      if (User::isUserSimple(Yii::$app->user->identity->id)){
            $this-> layout = 'mainUser';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
}

    /**
     * Creates a new CnvTipoDocumento model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
         if (User::isUserSimple(Yii::$app->user->identity->id)){
            $this-> layout = 'mainUser';
        $model = new CnvTipoDocumento();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_tipo_documento]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
         if (User::isUserCoordinador(Yii::$app->user->identity->id)){
            $this-> layout = 'mainCoord';
        $model = new CnvTipoDocumento();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_tipo_documento]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
         if (User::isUserAdmin(Yii::$app->user->identity->id)){
            $this-> layout = 'mainAdmin';
        $model = new CnvTipoDocumento();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_tipo_documento]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
}

    /**
     * Updates an existing CnvTipoDocumento model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {     
        if (User::isUserAdmin(Yii::$app->user->identity->id)){
            $this-> layout = 'mainAdmin';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_tipo_documento]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
     if (User::isUserCoordinador(Yii::$app->user->identity->id)){
            $this-> layout = 'mainCoord';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_tipo_documento]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
     if (User::isUserSimple(Yii::$app->user->identity->id)){
            $this-> layout = 'mainUser';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_tipo_documento]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
}

    /**
     * Deletes an existing CnvTipoDocumento model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (User::isUserSimple(Yii::$app->user->identity->id)){
            $this-> layout = 'mainUser';
        
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    if (User::isUserCoordinador(Yii::$app->user->identity->id)){
            $this-> layout = 'mainCoord';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    if (User::isUserAdmin(Yii::$app->user->identity->id)){
            $this-> layout = 'mainAdmin';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}

    /**
     * Finds the CnvTipoDocumento model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CnvTipoDocumento the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CnvTipoDocumento::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('La pagina solicitada no existe.');
        }
    }
}
