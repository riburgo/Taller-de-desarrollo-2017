<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CnvCoordinadorConvenioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Coordinador Convenios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cnv-coordinador-convenio-index">

    <h1>Buscar Coordinador Convenios</h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_coordinador_convenio',
            'rut_coordinador_convenio',
            'nombre_coordinador_convenio',
            'dv_coordinador_convenio',
            'fecha_inicio',
            // 'fecha_fin',
            // 'vigente',
            // 'esexterno',
            // 'unidad_academica',
            // 'email:email',
            // 'id_institucion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
