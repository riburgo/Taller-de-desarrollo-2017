<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\CnvConvenioInstitucion;
use app\models\CnvConvenio;
use app\models\CnvInstitucion;

/* @var $this yii\web\View */
/* @var $model app\models\CnvConvenioInstitucion */
/* @var $form yii\widgets\ActiveForm */
$idConvenios = ArrayHelper::map(CnvConvenio::find()->all(), 'id_convenio','id_convenio');
$idInstituciones = ArrayHelper::map(CnvInstitucion::find()->all(), 'id_institucion','id_institucion');

?>

<div class="cnv-convenio-institucion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_institucion')->dropDownList($idInstituciones) ?>

    <?= $form->field($model, 'id_convenio')->dropDownList($idConvenios) ?> 



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
