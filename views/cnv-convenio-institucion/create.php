<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CnvConvenioInstitucion */

$this->title = 'Asociar institucion con un Convenio';
$this->params['breadcrumbs'][] = ['label' => 'Cnv Convenio Institucions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cnv-convenio-institucion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
