<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CnvConvenioInstitucion */

$this->title = 'Update Cnv Convenio Institucion: ' . $model->id_institucion;
$this->params['breadcrumbs'][] = ['label' => 'Cnv Convenio Institucions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_institucion, 'url' => ['view', 'id_institucion' => $model->id_institucion, 'id_convenio' => $model->id_convenio]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cnv-convenio-institucion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
