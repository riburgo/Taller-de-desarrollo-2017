<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CnvConvenioInstitucion */

$this->title = $model->id_institucion;
$this->params['breadcrumbs'][] = ['label' => 'Cnv Convenio Institucions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cnv-convenio-institucion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id_institucion' => $model->id_institucion, 'id_convenio' => $model->id_convenio], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id_institucion' => $model->id_institucion, 'id_convenio' => $model->id_convenio], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_institucion',
            'id_convenio',
        ],
    ]) ?>

</div>
