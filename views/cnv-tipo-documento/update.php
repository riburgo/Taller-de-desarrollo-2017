<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CnvTipoDocumento */

$this->title = 'Modificar  Documento: ' . $model->id_tipo_documento;
$this->params['breadcrumbs'][] = ['label' => 'Modificiar Documentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_tipo_documento, 'url' => ['view', 'id' => $model->id_tipo_documento]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="cnv-tipo-documento-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
