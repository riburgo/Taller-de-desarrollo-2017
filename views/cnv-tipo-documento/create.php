<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CnvTipoDocumento */

$this->title = 'Agregar Nuevo Documento';
$this->params['breadcrumbs'][] = ['label' => 'Agregar Documento', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cnv-tipo-documento-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
