<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CnvConvenio */

$this->title = $model->id_convenio;
$this->params['breadcrumbs'][] = ['label' => 'Cnv Convenios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cnv-convenio-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_convenio], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_convenio], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_convenio',
            'id_tipo_convenio',
            'id_coordinador_convenio',
            'id_estado_convenio',
            'nombre_convenio',
            'fecha_inicio',
            'fecha_termino',
            'fecha_firma',
            'fecha_decreto',
            'numero_decreto',
            'descripcion',
            'vigente',
            'vigencia',
        ],
    ]) ?>

</div>
