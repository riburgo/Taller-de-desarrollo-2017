<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CnvActividadConvenio */

$this->title = 'Crear Actividad Convenio';
$this->params['breadcrumbs'][] = ['label' => 'Crear Actividad Convenio', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cnv-actividad-convenio-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
