<?php

use adminlte\widgets\Menu;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<aside class="main-sidebar">

    <section class="sidebar">
      
        <?=
        Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        ['label' => 'Menu', 'options' => ['class' => 'header']],
                        ['label' => 'Inicio','icon' => 'fa fa-home',   
                            'url' => ['/'], 'active' => $this->context->route == 'site/index'
                        ],
                         [
                            'label' => 'Convenios',
                            'icon' => 'fa fa-list',
                            'url' => '#',
                               'items' => [
                               
                                    [
                                         'label' => 'Buscar',
                                                'url' => ['/cnv-convenio']
                                    ],
                                     [
                                         'label' => 'Crear',
                                                'url' => ['/cnv-convenio/create']
                                    ],
                                    [
                                         'label' => 'Asociar instituciones',
                                                'url' => ['/cnv-convenio-institucion']
                                    ],
                            
                        ]
                        ],

                          [
                            'label' => 'Estado Convenio',
                            'icon' => 'fa fa-list',
                            'url' => '#',
                            'items' => [
                               
                                    [
                                         'label' => 'Buscar',
                                                'url' => ['/cnv-estado-convenio']
                                    ],
                                     [
                                         'label' => 'Crear',
                                                'url' => ['/cnv-estado-convenio/create']
                                    ],
                                
                            
                        ]
                        ],
                        [

                            'label' => 'Coordinador Convenio',
                            'icon' => 'fa fa-list',
                            'url' => '#',
                            'items' => [
                               
                                    [
                                         'label' => 'Buscar',
                                                'url' => ['/cnv-coordinador-convenio']
                                    ],
                                     [
                                         'label' => 'Crear',
                                                'url' => ['/cnv-coordinador-convenio/create']
                                    ],
                                
                            
                        ]


                        ],
                        [
                            'label' => 'Actividad de convenios',
                            'icon' => 'fa fa-list',
                            'url' => '#',
                            'items' => [

                                 [
                                    'label' => 'Consultar Actividad',
                                
                                    'url' => 'index.php?r=cnv-actividad-convenio', 
                    
                                ],

                                 [
                                    'label' => 'Consultar Responsables',
                                
                                    'url' => 'index.php?r=cnv-responsable-actividad', 
                    
                                ],
                                
                            ]
                        ],
                         [
                            'label' => 'Documentos convenios',
                            'icon' => 'fa fa-list',
                            'url' => '#',
                            'items' => [

                                 [
                                    'label' => 'Buscar',
                                
                                    'url' => 'index.php?r=cnv-tipo-documento', 
                    
                                ],

                                 [
                                    'label' => 'Crear',
                                
                                    'url' => 'index.php?r=cnv-tipo-documento/create', 
                    
                                ],
                                
                            ]
                        ],
                         [
                                    'label' => 'Convenio Institucion',
                                    'icon' => 'fa fa-list',
                                    'url' => '#',
                                     'items' => [

                                 [
                                    'label' => 'Buscar',
                                
                                    'url' => 'index.php?r=cnv-institucion', 
                    
                                ],

                                 [
                                    'label' => 'Crear',
                                
                                    'url' => 'index.php?r=cnv-institucion/create', 
                    
                                ],
                                
                            ]
                                ],
                                    ['label' => 'Acerca de','icon' => 'fa fa-plus', 'url' => ['/site/about'],],
                          ['label' => 'Contacto', 'icon' => 'fa fa-tag','url' => ['/site/contact'],],

                    
                       
                    ],
                ]
        )

        ?>
        
    </section>
    <!-- /.sidebar -->
</aside>
