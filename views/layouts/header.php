<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
?>
<header class="main-header">
        <!-- Logo -->
        <a href="index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>UM</b></span>
          <!-- logo for regular state and mobile devices -->
           <span class="logo-lg"><b><UM></b> Universidad Macondo</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">

          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
          
              <div class="wrap">
              <?php
                echo Nav::widget([
        'options' => ['class' => 'navbar-nav left'],
        'items' => [
            Yii::$app->user->isGuest ? (

                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-success logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);

    
    ?>

 
</div>

              
           
           
          </div>
        </nav>
      </header>
